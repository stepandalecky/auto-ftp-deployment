# Auto FTP Deployment

[David Grudl's ftp-deployment](https://github.com/dg/ftp-deployment) triggered by GitLab CI.

## Setup
Simply add following lines into `.gitlab-ci.yml` file in your project root.

```yaml
include: https://gitlab.com/stepandalecky/auto-ftp-deployment/raw/master/ftp-deployment-template.yml

ftp-deployment: # Job name cannot be modified.
  variables:
    DEPLOYMENT_CONFIG_FILE: ./deployment-config.php # Path to your deployment config file.
  stage: deploy
  only:
    - master # Deployment will run every time you merge to master.
```

If you don't have a deployment config file yet, create one and fill its path as the value of _DEPLOYMENT_CONFIG_FILE_ variable.
See https://github.com/dg/ftp-deployment how to configure the deployment process.

You likely don't want to version your FTP credentials, so it's recommended to create a PHP config file where you're able
to use environment variables set in GitLab. The credentials will stay in secret.
In this case, you may create file _deployment-config.php_ with following content:
```php
<?php

return [
	'remote' => getenv('FTP_REMOTE_URL'), // ftp://user:password@ftp-host/dir
	'local' => 'content', // Relative path to the folder you want to upload.
];

// See https://github.com/dg/ftp-deployment for more config options.
```
Set the value of `FTP_REMOTE_URL` variable in GitLab in _Settings_ > _CI/CD_ > _Variables_.
