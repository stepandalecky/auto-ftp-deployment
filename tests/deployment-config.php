<?php

return [
	'remote' => getenv('FTP_REMOTE_URL'), // ftp://user:password@ftp-host/dir
	'local' => 'content', // Relative path to the folder you want to upload.
];

// See https://github.com/dg/ftp-deployment for more config options.
