<?php
declare(strict_types=1);

try {
	$url = getenv('FTP_REMOTE_URL');
	$testFileName = 'test-file.txt';
	$testFilePath = __DIR__ . '/content/' . $testFileName;

	$url = parse_url($url);
	$connection = $url['scheme'] === 'ftp'
		? ftp_connect($url['host'], $url['port'] ?? 21)
		: ftp_ssl_connect($url['host'], $url['port'] ?? 21);
	ftp_login($connection, urldecode($url['user']), urldecode($url['pass']));

	ftp_set_option($connection, FTP_USEPASVADDRESS, false);
	ftp_pasv($connection, true);

	ftp_chdir($connection, $url['path']);
	ftp_get($connection, $testFileName, $testFileName, FTP_BINARY);
	$ftpFileContent = file_get_contents($testFileName);
	$originalFileContent = file_get_contents($testFilePath);
	if ($ftpFileContent !== $originalFileContent) {
		throw new \Exception(sprintf('Test files do not match: "%s" vs. "%s"', $ftpFileContent, $originalFileContent));
	}

	ftp_delete($connection, $testFileName);
	ftp_delete($connection, '.htdeployment');

	echo "\n\nTest successful\n\n";
	exit(0);
} catch (\Exception $e) {
	echo $e->getMessage() . "\n";
	exit(1);
}
